library storage;

import 'package:hive_flutter/hive_flutter.dart';

class Storage {
  static late Box box;

  static Future<void> init() async {
    await Hive.initFlutter();
    box = await Hive.openBox('Hack');
  }

  static final Storage shared = Storage._internal();

  Storage._internal();

  void setRefreshToken(String refreshToken) {
    box.put(StorageKey.refreshToken, refreshToken);
  }

  String? getRefreshToken() {
    return box.get(StorageKey.refreshToken);
  }

  void setToken(String token) {
    box.put(StorageKey.token, token);
  }

  void removeToken() {
    box.delete(StorageKey.token);
  }

  void removeRefreshToken() {
    box.delete(StorageKey.refreshToken);
  }

  String? getToken() {
    return box.get(StorageKey.token);
  }
}

class StorageKey {
  static String token = 'token';
  static String refreshToken = "refreshToken";
}
