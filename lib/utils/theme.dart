import 'package:flutter/material.dart';

class ThemeColor {
  static const warning = Color(0xffF87E58);
  static const inProcess = Color(0xffFBAF1D);
  static const done = Color(0xff42B674);
  static const action = Color(0xffFFE486);
  static const finish = Color(0xffFDE7D9);
  static const disable = Color(0xffD7D6D6);
  static const backgroundTile = Color(0xffF5F5F5);
  static const addPlus = Color(0xff006B8C);
  static const darkText = Color(0xff2D3E3F);

  static const int mainColor = 0xFF2F6051;
  static const MaterialColor mainMaterialColor = MaterialColor(
    mainColor,
    <int, Color>{
      50: Color.fromRGBO(47, 96, 81, .1),
      100: Color.fromRGBO(47, 96, 81, .2),
      200: Color.fromRGBO(47, 96, 81, .3),
      300: Color.fromRGBO(47, 96, 81, .4),
      400: Color.fromRGBO(47, 96, 81, .5),
      500: Color.fromRGBO(47, 96, 81, .6),
      600: Color.fromRGBO(47, 96, 81, .7),
      700: Color.fromRGBO(47, 96, 81, .8),
      800: Color.fromRGBO(47, 96, 81, .9),
      900: Color.fromRGBO(47, 96, 81, 1),
    },
  );
}

class SvgIcon {
  static String addTask = 'assets/svg/addTask.svg';
  static String inProgressTask = 'assets/svg/inProgressTask.svg';
  static String doneTask = 'assets/svg/doneTask.svg';
  static String pauseTask = 'assets/svg/pauseTask.svg';
  static String calendar = 'assets/svg/calendar.svg';
  static String phone = 'assets/svg/phone.svg';
  static String notify = 'assets/svg/notify.svg';
  static String bnbActiveTaskActive = 'assets/svg/bnbActiveTaskActive.svg';
  static String bnbTasks = 'assets/svg/bnbTasks.svg';
  static String bnbProfile = 'assets/svg/bnbProfile.svg';
  static String bnbActiveTask = 'assets/svg/bnbActiveTask.svg';
  static String bnbTasksActive = 'assets/svg/bnbTasksActive.svg';
  static String bnbProfileActive = 'assets/svg/bnbProfileActive.svg';
  static String exit = 'assets/svg/exit.svg';
  static String settings = 'assets/svg/settings.svg';
  static String search = 'assets/svg/search.svg';
}

class PngIcon {
  static String marker = 'assets/png/Splash Screen agrocode_hack_mobile.png';
}

class StyleText {
  static const TextStyle normalSmall = TextStyle(
    color: ThemeColor.darkText,
    fontWeight: FontWeight.w400,
    fontSize: 12.0,
    fontFamily: 'Rubik',
    height: 1.185,
  );

  static const TextStyle normalMedium = TextStyle(
    color: ThemeColor.mainMaterialColor,
    fontWeight: FontWeight.w400,
    fontSize: 14.0,
    fontFamily: 'Rubik',
    height: 1.185,
  );

  static const TextStyle normalLarge = TextStyle(
    color: ThemeColor.darkText,
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
    fontFamily: 'Rubik',
    height: 1.185,
  );

  static const TextStyle h1Bold = TextStyle(
    color: ThemeColor.darkText,
    fontWeight: FontWeight.w500,
    fontSize: 20.0,
    fontFamily: 'Rubik',
    height: 1.185,
  );
  static const TextStyle boldLarge = TextStyle(
    color: ThemeColor.darkText,
    fontWeight: FontWeight.w500,
    fontSize: 16.0,
    fontFamily: 'Rubik',
    height: 1.185,
  );
}
