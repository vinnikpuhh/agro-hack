import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class RouterCfg {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Initial.routeName:
        return PageTransition(
          child: const Initial(),
          type: PageTransitionType.fade,
          settings: settings,
        );
      case AuthView.routeName:
        return PageTransition(
          child: const AuthView(),
          type: PageTransitionType.fade,
          settings: settings,
        );
      case ActualTaskView.routeName:
        return PageTransition(
          child: const ActualTaskView(),
          type: PageTransitionType.fade,
          settings: settings,
        );
      case OrdersView.routeName:
        return PageTransition(
          child: const OrdersView(),
          type: PageTransitionType.fade,
          settings: settings,
        );

      case ProfileView.routeName:
        return PageTransition(
          child: const ProfileView(),
          type: PageTransitionType.fade,
          settings: settings,
        );

      default:
        return PageTransition(
          child: const AuthView(),
          type: PageTransitionType.fade,
          settings: settings,
        );
    }
  }
}
