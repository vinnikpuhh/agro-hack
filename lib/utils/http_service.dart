// ignore_for_file: avoid_print

library http_client;

import 'package:agrocode_hack_mobile/utils/storage.dart';
import 'package:dio/dio.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

enum EnvironmentType { production, development }

class Environment {
  static const EnvironmentType env = EnvironmentType.development;

  ///TODO адрес куда стучимся!
  static String getApi() => env == EnvironmentType.development
      ? "http://192.168.0.125:8000"
      : "http://31.129.111.98:8000";
}

class DioClient {
  final Dio _dio = Dio();
  final void Function() onRefreshFailure;
  Future<bool>? _updating;

  DioClient({required this.onRefreshFailure}) {
    _dio.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      compact: false,
    ));

    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) async {
          var token = Storage.shared.getToken();

          if (token == null || !Jwt.isExpired(token)) {
            _replaceClientId(options, token: token);
            _headers(options, token: token);
          } else {
            print('updating tokens');
            //this is only executed when token != null and expired
            if (_updating == null) {
              _updating ??= _refreshToken();

              final res = await _updating!;
              _updating = null;
              if (res) {
                print('refresh success');
                token = Storage.shared.getToken();
                _replaceClientId(options, token: token);
                _headers(options, token: token);
              } else {
                print('refresh failed!');
                Storage.shared.removeToken();
                Storage.shared.removeRefreshToken();
                onRefreshFailure();
                return handler.resolve(Response(
                  requestOptions: options,
                  data: null,
                  statusCode: 401,
                ));
              }
            } else {
              if (await _updating!) {
                print('refresh success');
                token = Storage.shared.getToken();
                _replaceClientId(options, token: token);
                _headers(options, token: token);
              } else {
                print('refresh failed!');
                Storage.shared.removeToken();
                Storage.shared.removeRefreshToken();
                onRefreshFailure();
                return handler.resolve(Response(
                  requestOptions: options,
                  data: null,
                  statusCode: 401,
                ));
              }
            }
          }

          return handler.next(options);
        },
      ),
    );
  }

  void _replaceClientId(RequestOptions options, {String? token}) {
    if (token != null) {
      var clientId = Jwt.parseJwt(token)['sub'];

      options.path = options.path.replaceAll('{clientId}', clientId);
      print(clientId);
    }
  }

  void _headers(RequestOptions options, {String? token}) async {
    if (token != null) {
      options.headers['Authorization'] = 'Bearer $token';
    }

    options.headers['Content-type'] = 'application/json';
    options.headers['Accept'] = 'application/json';
    options.headers['Accept-Charset'] = 'utf-8';
  }

  Future<bool> _refreshToken() async {
    try {
      final Dio refreshDio = Dio();
      refreshDio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        compact: false,
      ));
      final res = await refreshDio
          .post(_getFullPath('/v1/mobile/clients/sign-in/refresh/'), data: {
        "refreshToken": Storage.shared.getRefreshToken(),
      });

      if ((res.statusCode ?? -1) >= 200 && (res.statusCode ?? -1) < 300) {
        Storage.shared.setToken(res.data['accessToken']);
        Storage.shared.setRefreshToken(res.data['refreshToken']);
        return true;
      }

      return false;
    } catch (e) {
      return false;
    }
  }

  String _getFullPath(String relative) {
    if (relative.startsWith('/')) {
      return '${Environment.getApi()}$relative';
    }

    return '${Environment.getApi()}/$relative';
  }

  Future<Response<T>> get<T>(
    String relativePath, {
    Map<String, dynamic>? queryParameters,
  }) {
    return _dio.get(
      _getFullPath(relativePath),
      queryParameters: queryParameters,
    );
  }

  Future<Response<T>> post<T>(
    String relativePath, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
  }) {
    return _dio.post(
      _getFullPath(relativePath),
      queryParameters: queryParameters,
      data: data,
    );
  }

  Future<Response<T>> put<T>(
    String relativePath, {
    data,
    Map<String, dynamic>? queryParameters,
  }) {
    return _dio.put(
      _getFullPath(relativePath),
      queryParameters: queryParameters,
      data: data,
    );
  }

  Future<Response<T>> delete<T>(
    String relativePath, {
    data,
    Map<String, dynamic>? queryParameters,
  }) {
    return _dio.delete(
      _getFullPath(relativePath),
      queryParameters: queryParameters,
      data: data,
    );
  }

  Future<Response<T>> patch<T>(
    String relativePath, {
    data,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await _dio.patch(
      _getFullPath(relativePath),
      queryParameters: queryParameters,
      data: data,
    );
  }
}
