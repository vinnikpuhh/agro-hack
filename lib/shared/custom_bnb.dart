import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomBottomNavigation extends StatefulWidget {
  final String? currentRoute;
  const CustomBottomNavigation({
    super.key,
    required this.currentRoute,
  });

  @override
  State<CustomBottomNavigation> createState() => _CustomBottomNavigationState();
}

class _CustomBottomNavigationState extends State<CustomBottomNavigation> {
  int selectedIndex = 0;

  List items = [
    BottomNavItem(
      routeName: ActualTaskView.routeName,
      icon: SvgIcon.bnbActiveTask,
      selectedIcon: SvgIcon.bnbActiveTaskActive,
    ),
    BottomNavItem(
      routeName: OrdersView.routeName,
      icon: SvgIcon.bnbTasks,
      selectedIcon: SvgIcon.bnbTasksActive,
    ),
  ];

  @override
  Widget build(BuildContext ctx) {
    return NavigationBarTheme(
      data: const NavigationBarThemeData(
        indicatorColor: Colors.transparent,
        backgroundColor: ThemeColor.mainMaterialColor,
      ),
      child: Container(
        color: ThemeColor.mainMaterialColor,
        child: Row(
          children: [
            const Spacer(),
            Expanded(
              flex: 3,
              child: NavigationBar(
                animationDuration: const Duration(milliseconds: 500),
                height: MediaQuery.of(context).size.height * 0.08,
                onDestinationSelected: (int index) {
                  selectedIndex = index;
                  Navigator.popAndPushNamed(
                      context, items[selectedIndex].routeName);
                  setState(() {});
                },
                selectedIndex: widget.currentRoute != null
                    ? items.indexWhere(
                        (i) => widget.currentRoute!.contains(i.routeName))
                    : 0,
                destinations: items
                    .map(
                      (e) => ItemNavBar(
                        routeName: e.routeName,
                        icon: e.icon,
                        selectedIcon: e.selectedIcon,
                      ),
                    )
                    .toList(),
              ),
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}

class ItemNavBar extends StatelessWidget {
  final String icon;
  final String selectedIcon;
  final String routeName;

  const ItemNavBar({
    super.key,
    required this.icon,
    required this.selectedIcon,
    required this.routeName,
  });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: NavigationDestination(
        icon: SvgPicture.asset(icon),
        label: '',
        selectedIcon: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: SvgPicture.asset(icon),
            ),
            Align(
              alignment: Alignment.center,
              child: SvgPicture.asset(selectedIcon),
            ),
          ],
        ),
      ),
    );
  }
}

class BottomNavItem {
  final String icon;
  final String selectedIcon;
  final String routeName;

  BottomNavItem({
    required this.icon,
    required this.selectedIcon,
    required this.routeName,
  });
}
