import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';

class CustomChip extends StatelessWidget {
  const CustomChip({
    super.key,
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30.0,
      padding: const EdgeInsets.symmetric(vertical: 5.5, horizontal: 8.0),
      decoration: BoxDecoration(
        border: Border.all(color: ThemeColor.addPlus),
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: Text(
        title,
        style: StyleText.boldLarge,
      ),
    );
  }
}
