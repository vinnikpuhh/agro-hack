import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';

class InfoBox extends StatelessWidget {
  const InfoBox({
    super.key,
    required this.title,
    required this.description,
    this.customColor,
  });

  final Color? customColor;
  final String title;
  final String description;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 51.0,
      padding: const EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 16.0,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16.0),
        color: customColor ?? ThemeColor.backgroundTile,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: StyleText.normalSmall,
          ),
          Text(
            description,
            style: StyleText.normalMedium,
          ),
        ],
      ),
    );
  }
}
