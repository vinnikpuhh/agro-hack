/* import 'dart:math' as math;
import 'dart:async';
import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';

class CalendarHeader extends StatefulWidget {
  final bool showShadow;

  const CalendarHeader({
    Key? key,
    required this.showShadow,
  }) : super(key: key);

  @override
  _CalendarHeaderState createState() => _CalendarHeaderState();
}

class _CalendarHeaderState extends State<CalendarHeader>
    with WidgetsBindingObserver {
  final int extent = 365;
  final scrollController = ScrollController();
  late DateTime currentlyHovered;
/*   late MealsContainer mc; */
  late MediaQueryData mq;

  double get screenWidth {
    return mq.size.width;
  }

  double get itemWidth {
    return mq.size.width / 6;
  }

  int get centerItem {
    int item = 0;
    final itemWidth = this.itemWidth;
    var offset = scrollController.offset + mq.size.width / 2;

    while (true) {
      if (offset > itemWidth) {
        item++;
        offset -= itemWidth;
      } else {
        return item;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
/*     currentlyHovered = context.read<MealsContainer>().selectedDate; */
    currentlyHovered = DateTime.now();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      scrollController.jumpTo(
        scrollPositionFromDate(/* mc.selectedDate */ DateTime.now()),
      );
    });
    /*  BlocProvider.of<StoriesCubit>(context).load(); */
  }

  @override
  void didChangeDependencies() {
    mq = MediaQuery.of(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
/* 
    if (state == AppLifecycleState.resumed) {
      if (Get.currentRoute == HomeScreen.routeName) {
        currentlyHovered = DateTime.now().alignDown();
        setProviderDateAndFetch(currentlyHovered);

        animateScroll(
          scrollPositionFromDate(mc.selectedDate),
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeInOutCubic,
        );
      }
    } else if (state == AppLifecycleState.paused) {
      mc.comments.done();
    } */
  }

  void animateScroll(
    double to, {
    required Duration duration,
    required Curve curve,
  }) {
    scrollController.animateTo(
      to,
      duration: duration,
      curve: curve,
    );
  }

  /*  SmoothBorderRadius get borderRadius {
    return const SmoothBorderRadius.only(
      bottomLeft: SmoothRadius(
        cornerRadius: 24,
        cornerSmoothing: 1.0,
      ),
      bottomRight: SmoothRadius(
        cornerRadius: 24,
        cornerSmoothing: 1.0,
      ),
    );
  } */

  @override
  Widget build(BuildContext context) {
/*     mc = context.watch<MealsContainer>(); */
    return Material(
      /*     shape: SmoothRectangleBorder(
        borderRadius: borderRadius,
      ), */
      shadowColor: /* FTTheme.shadowColor */ Colors.grey,
      elevation: widget.showShadow ? 5.0 : 0.0,
      clipBehavior: Clip.antiAlias,
      color: Colors.white,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          width: double.infinity,
          height: 132.0,
          padding: const EdgeInsets.symmetric(
            vertical: 14.0,
          ),
          child: Column(
            children: <Widget>[
              /*   _CalendarTitle(
                currentlyHovered: currentlyHovered,
              ),
              const SizedBox(height: 12), */
              Expanded(child: calendar()),
            ],
          ),
        ),
      ),
    );
  }

  Widget calendar() {
    return Listener(
      onPointerUp: (event) {
        FocusScope.of(context).unfocus();
        /*  mc.comments.done(); */
        currentlyHovered = selectedDate(centerItem);
        setProviderDateAndFetch(currentlyHovered);

        animateScroll(
          scrollPositionFromDate(/* mc.selectedDate */ DateTime.now()),
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeInOutCubic,
        );
      },
      behavior: HitTestBehavior.translucent,
      child: ScrollConfiguration(
        behavior: ScrollConfiguration.of(context).copyWith(
          scrollbars: false,
        ),
        child: calendarItemList(),
      ),
    );
  }

  Widget calendarItemList() {
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: Stack(
        children: [
          SizedBox.expand(
            child: ListView.builder(
              physics: const _NonFlingableScrollPhysics(),
              scrollDirection: Axis.horizontal,
              controller: scrollController,
              itemCount: 2 * extent + 1,
              itemExtent: itemWidth,
              itemBuilder: (ctx, index) {
                final now = DateTime.now();
                final dt = DateTime(
                  now.year,
                  now.month,
                  now.day + index - extent,
                );

                return _CalendarItem(
                  screenWidth: screenWidth,
                  thisTime: dt,
                  onTap: () {
                    FocusScope.of(context).unfocus();
                    /*  mc.comments.done(); */
                    setProviderDateAndFetch(dt);
                    animateScroll(
                      scrollPositionFromDate(dt),
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.easeInOutCubic,
                    );
                    setState(() {});
                  },
                  currentlyHovered: currentlyHovered,
                  isToday: now.day == dt.day &&
                      now.month == dt.month &&
                      now.year == dt.year,
                );
              },
            ),
          ),
          IgnorePointer(
            ignoring: true,
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Colors.white,
                    Colors.white.withOpacity(0.0),
                    Colors.white.withOpacity(0.0),
                    Colors.white,
                  ],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  stops: const [0.0, 0.10, 0.90, 1.0],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  DateTime selectedDate(int offsetInDays) {
    return DateTime(DateTime.now().year, DateTime.now().month,
        DateTime.now().day + offsetInDays - extent);
  }

  void setProviderDateAndFetch(DateTime dt) {
    /*   if (/* mc.selectedDate.isAtSameMomentAs(dt) */) {
      return;
    } */

    /*   mc.selectedDate = dt;
    var beforeEnd =
        mc.rightBoundary != null && mc.selectedDate.isBefore(mc.rightBoundary!);
    var afterStart =
        mc.leftBoundary != null && mc.selectedDate.isAfter(mc.leftBoundary!);
    if (beforeEnd && afterStart) {
      return;
    }

    mc.fetchMeals(
      start: DateTime(dt.year, dt.month, dt.day - 7),
      end: DateTime(dt.year, dt.month, dt.day + 7),
      overrideBoundaries: false,
    ); */
  }

  double scrollPositionFromDate(DateTime selected) {
    final today = DateTime.now() /* .alignDown() */;
    final leftBoundary = DateTime(
      today.year,
      today.month,
      today.day - extent + 3,
    );
    final rightBoundary = DateTime(
      today.year,
      today.month,
      today.day + extent - 3,
    );

    if (selected.isBefore(leftBoundary)) {
      currentlyHovered = leftBoundary;
      return 0.0;
    }

    if (selected.isAfter(rightBoundary)) {
      currentlyHovered = rightBoundary;
      return scrollController.position.maxScrollExtent;
    }

    final leftAlign = leftBoundary;

    var count = 0;
    while (true) {
      final tmp =
          DateTime(leftAlign.year, leftAlign.month, leftAlign.day + count);
      if (tmp == selected) {
        currentlyHovered = tmp;
        return itemWidth * (count + .5);
      }
      count++;
    }
  }
}

class _CalendarItem extends StatelessWidget {
  final VoidCallback onTap;
  final double screenWidth;
  final DateTime thisTime;
  final DateTime currentlyHovered;
  final bool isToday;

  const _CalendarItem({
    Key? key,
    required this.screenWidth,
    required this.onTap,
    required this.thisTime,
    required this.currentlyHovered,
    required this.isToday,
  }) : super(key: key);

  double get itemWidth {
    return screenWidth / 8;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap,
      child: SizedBox(
        width: itemWidth,
        height: double.infinity,
        child: Center(
          child: AnimatedSwitcher(
            duration: const Duration(milliseconds: 450),
            switchInCurve: Curves.easeInOutCubic,
            switchOutCurve: Curves.easeInOutCubic,
            child: child(),
          ),
        ),
      ),
    );
  }

  Widget child() {
    final cond = thisTime.isAtSameMomentAs(currentlyHovered);
    return LayoutBuilder(
      builder: (_, ctrx) {
        return day(ctrx, cond);
      },
    );
  }

  Widget day(BoxConstraints ctrx, bool selected) {
    final width = ctrx.maxHeight * 0.8;
    final padding = math.max(5, ctrx.maxWidth - width) / 2;

    return SizedBox.expand(
      child: Stack(
        children: [
          Container(
            key: ValueKey(thisTime.isAtSameMomentAs(currentlyHovered)),
            height: double.infinity,
            width: double.infinity,
            margin: EdgeInsets.only(
              bottom: 3,
              right: padding,
              left: padding,
              top: 1,
            ),
            /*   decoration: ShapeDecoration(
             /*  gradient: selected /* ? FTTheme.orange.gradient : null */ */
              color: !selected ? Colors.white : Colors.amber,
             /*  shape: SmoothRectangleBorder(
                borderRadius: const SmoothBorderRadius.all(
                  SmoothRadius(
                    cornerRadius: 12,
                    cornerSmoothing: 1.0,
                  ),
                ),
                side: !selected
                    ? BorderSide(
                        color: FTTheme.grey.shade500,
                        width: 1.0,
                      )
                    : BorderSide.none,
              ), */
            ), */
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  dayOfWeek(thisTime.weekday),
                  /*  style: FTTheme.textTheme.regular12px.copyWith(
                    color: selected ? Colors.white : FTTheme.grey.shade600,
                  ), */
                ),
                Text(
                  thisTime.day.toString(),
                  /*  style: FTTheme.textTheme.accent14px.copyWith(
                    color: selected ? Colors.white : FTTheme.black.shade500,
                  ), */
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Visibility(
              visible: isToday,
              child: const _TodayMark(),
            ),
          ),
        ],
      ),
    );
  }

  String dayOfWeek(int num) {
    return ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'][num - 1];
  }
}

class _TodayMark extends StatelessWidget {
  const _TodayMark({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 8,
      width: 8,
      decoration: BoxDecoration(
        color: ThemeColor.mainMaterialColor,
        shape: BoxShape.circle,
      ),
      child: Center(
        child: Container(
          height: 6,
          width: 6,
          decoration: const BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
          ),
          child: Center(
            child: Container(
              height: 4,
              width: 4,
              decoration: BoxDecoration(
                color: ThemeColor.mainMaterialColor,
                shape: BoxShape.circle,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
/* 
class _CalendarTitle extends StatelessWidget {
  final DateTime currentlyHovered;

  const _CalendarTitle({
    Key? key,
    required this.currentlyHovered,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Stack(
        children: [
          Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SvgPicture.asset(
                  FTTheme.newIcons.calendar,
                ),
                const SizedBox(width: 8.0),
                Text(
                  currentlyHovered.russianMonthNameNominative,
                  style: FTTheme.textStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
          ),
          const Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 16.0,
              ),
              child: NotificationsButton(),
            ),
          ),
        ],
      ),
    );
  }
} */

class _NonFlingableScrollPhysics extends ScrollPhysics {
  const _NonFlingableScrollPhysics({ScrollPhysics? parent})
      : super(parent: parent);

  @override
  _NonFlingableScrollPhysics applyTo(ScrollPhysics? ancestor) {
    return _NonFlingableScrollPhysics(parent: buildParent(ancestor));
  }

  @override
  Simulation? createBallisticSimulation(
      ScrollMetrics position, double velocity) {
    return null;
  }

  @override
  bool get allowImplicitScrolling => false;
}
 */