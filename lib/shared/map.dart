import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapGoogle extends StatelessWidget {
  const MapGoogle({super.key, required this.workArea});

  final WorkArea? workArea;
  //TODO Прокинуть точку задачи на initialCameraPosition

  @override
  Widget build(BuildContext context) {
    return googleMap();
  }

  Widget googleMap() {
    return GoogleMap(
      mapToolbarEnabled: false,
      markers: {
        Marker(
          markerId: MarkerId('marker'),
          position: LatLng(double.parse(workArea?.latitude ?? "0.0"),
              double.parse(workArea?.longitude ?? "0.0")),
        ),
      },
      zoomControlsEnabled: false,
      myLocationButtonEnabled: false,
      rotateGesturesEnabled: true,
      scrollGesturesEnabled: true,
      zoomGesturesEnabled: true,
      compassEnabled: false,
      initialCameraPosition: CameraPosition(
        target: LatLng(double.parse(workArea?.latitude ?? "0.0"),
            double.parse(workArea?.longitude ?? "0.0")),
        zoom: 15.0,
      ),
      onMapCreated: (controller) async {},
      myLocationEnabled: true,
      onCameraMoveStarted: () {},
    );
  }
}
