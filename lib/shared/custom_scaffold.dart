import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';
import 'index.dart';

class CustomScaffold extends StatelessWidget {
  const CustomScaffold({
    super.key,
    required this.child,
    required this.currentRoute,
  });

  final Widget child;
  final String currentRoute;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: child,
      bottomNavigationBar: currentRoute == AuthView.routeName
          ? null
          : CustomBottomNavigation(
              currentRoute: currentRoute,
            ),
    );
  }
}
