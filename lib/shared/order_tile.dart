import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OrderTile extends StatelessWidget {
  const OrderTile({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        openBS(
          context: context,
          heightBS: MediaQuery.of(context).size.height * 0.7,
          width: double.infinity,
          child: Container(),
        );
      },
      behavior: HitTestBehavior.opaque,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
        height: 75.0,
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16.0),
          border: Border.all(
            width: 0.5,
            color: ThemeColor.mainMaterialColor,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Посев',
                  style: StyleText.normalLarge,
                ),
                SvgPicture.asset(SvgIcon.addTask),
              ],
            ),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Вспашка земли'),
                Text('А-121-АБ'),
                Text('RSM 3575'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
