import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';

Future openBS({
  required BuildContext context,
  required double? heightBS,
  required double width,
  required Widget child,
  Color? shadow,
}) async {
  return showModalBottomSheet(
    isScrollControlled: true,
    enableDrag: true,
    useRootNavigator: true,
    barrierColor: shadow ?? Colors.transparent,
    backgroundColor: Colors.transparent,
    context: context,
    builder: (BuildContext ctx) {
      return Column(
        children: [
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                Navigator.pop(context);
              },
              child: const SizedBox.expand(),
            ),
          ),
          Container(
            height: 6,
            width: 60,
            decoration: BoxDecoration(
              color: ThemeColor.action,
              borderRadius: BorderRadius.circular(5.0),
            ),
          ),
          const SizedBox(height: 3.0),
          Container(
            width: width,
            height: heightBS,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40.0),
                topRight: Radius.circular(40.0),
              ),
            ),
            padding: const EdgeInsets.all(24.0),
            child: child,
          ),
        ],
      );
    },
  );
}
