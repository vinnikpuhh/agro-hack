import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  const ActionButton({
    super.key,
    required this.color,
    required this.title,
    required this.onTap,
    this.loading,
  });

  final void Function()? onTap;
  final String title;
  final Color color;
  final bool? loading;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
      child: Container(
        height: 40.0,
        alignment: Alignment.center,
        width: double.infinity,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(
            16.0,
          ),
        ),
        child: loading ?? false
            ? SizedBox(
                height: 30.0, width: 30.0, child: CircularProgressIndicator())
            : Text(
                title,
                style: StyleText.normalLarge.copyWith(
                    color: color == ThemeColor.mainMaterialColor
                        ? Colors.white
                        : null),
              ),
      ),
    );
  }
}
