import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';

class Initial extends StatefulWidget {
  const Initial({super.key});

  static const String routeName = '/initial';

  @override
  InitialState createState() => InitialState();
}

class InitialState extends State<Initial> {
  late String token;
  @override
  void initState() {
    super.initState();
    token = Storage.shared.getToken() ?? '';
    // ignore: avoid_print
    print(token);
    Future(() => cycleDoneCallback());
  }

  void cycleDoneCallback() {
    if (token.isEmpty) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(AuthView.routeName, (_) => false);

      return;
    } else {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(ActualTaskView.routeName, (_) => false);
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          color: Colors.white,
        ),
      ),
    );
  }
}
