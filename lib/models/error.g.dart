// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ErrorModelImpl _$$ErrorModelImplFromJson(Map<String, dynamic> json) =>
    _$ErrorModelImpl(
      errorId: json['errorId'] as int,
      id: json['id'] as int,
      errorMessage: json['errorMessage'] as String,
    );

Map<String, dynamic> _$$ErrorModelImplToJson(_$ErrorModelImpl instance) =>
    <String, dynamic>{
      'errorId': instance.errorId,
      'id': instance.id,
      'errorMessage': instance.errorMessage,
    };
