class TokenResponse {
  final String? token;
  final String? refreshToken;

  const TokenResponse({this.token, this.refreshToken});

  factory TokenResponse.fromJson(Map<String, dynamic> json) {
    return TokenResponse(
      token: json['access_token'],
      refreshToken: json['refresh_token'],
    );
  }
}
