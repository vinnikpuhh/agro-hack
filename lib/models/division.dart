import 'package:agrocode_hack_mobile/index.dart';

class Division {
  String? id;
  String? name;
  List<User>? users;
  List<WorkArea>? workArea;
  List<WorkingTools>? workingTools;

  Division({this.id, this.name, this.users, this.workArea, this.workingTools});

  Division.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    if (json['users'] != null) {
      users = <User>[];
      json['users'].forEach((v) {
        users!.add(new User.fromJson(v));
      });
    }
    if (json['work_area'] != null) {
      workArea = <WorkArea>[];
      json['work_area'].forEach((v) {
        workArea!.add(new WorkArea.fromJson(v));
      });
    }
    if (json['working_tools'] != null) {
      workingTools = <WorkingTools>[];
      json['working_tools'].forEach((v) {
        workingTools!.add(new WorkingTools.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    if (this.users != null) {
      data['users'] = this.users!.map((v) => v.toJson()).toList();
    }
    if (this.workArea != null) {
      data['work_area'] = this.workArea!.map((v) => v.toJson()).toList();
    }
    if (this.workingTools != null) {
      data['working_tools'] =
          this.workingTools!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
