class WorkArea {
  String? name;
  String? description;
  String? latitude;
  String? longitude;
  String? areaSize;
  String? units;
  String? id;

  WorkArea(
      {this.name,
      this.description,
      this.latitude,
      this.longitude,
      this.areaSize,
      this.units,
      this.id});

  WorkArea.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    description = json['description'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    areaSize = json['area_size'];
    units = json['units'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['description'] = this.description;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['area_size'] = this.areaSize;
    data['units'] = this.units;
    data['id'] = this.id;
    return data;
  }
}