class TaskParameters {
  String? name;
  String? value;
  String? units;
  String? id;
  String? taskId;

  TaskParameters({this.name, this.value, this.units, this.id, this.taskId});

  TaskParameters.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
    units = json['units'];
    id = json['id'];
    taskId = json['task_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    data['units'] = this.units;
    data['id'] = this.id;
    data['task_id'] = this.taskId;
    return data;
  }
}
