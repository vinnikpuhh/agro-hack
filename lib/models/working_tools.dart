class WorkingTools {
  String? name;
  String? userId;
  String? categoryId;
  String? id;

  WorkingTools({this.name, this.userId, this.categoryId, this.id});

  WorkingTools.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    userId = json['user_id'];
    categoryId = json['category_id'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['user_id'] = this.userId;
    data['category_id'] = this.categoryId;
    data['id'] = this.id;
    return data;
  }
}
