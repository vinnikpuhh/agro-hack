import 'package:freezed_annotation/freezed_annotation.dart';
part 'error.freezed.dart';
part 'error.g.dart';

@Freezed()
class ErrorModel with _$ErrorModel {
  const factory ErrorModel({
    required int errorId,
    required int id,
    required String errorMessage,
  }) = _ErrorModel;

  factory ErrorModel.fromJson(Map<String, dynamic> json) =>
      _$ErrorModelFromJson(json);
}
