class User {
  String? id;
  String? email;
  String? username;
  String? phoneNumber;
  bool? isActive;
  String? role;
  Profile? profile;

  User(
      {this.id,
      this.email,
      this.username,
      this.phoneNumber,
      this.isActive,
      this.role,
      this.profile});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    username = json['username'];
    phoneNumber = json['phone_number'];
    isActive = json['is_active'];
    role = json['role'];
    profile =
        json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['username'] = this.username;
    data['phone_number'] = this.phoneNumber;
    data['is_active'] = this.isActive;
    data['role'] = this.role;
    if (this.profile != null) {
      data['profile'] = this.profile!.toJson();
    }
    return data;
  }
}

class Profile {
  String? firstName;
  String? middleName;
  String? lastName;
  String? birthday;
  String? timezone;

  Profile(
      {this.firstName,
      this.middleName,
      this.lastName,
      this.birthday,
      this.timezone});

  Profile.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    middleName = json['middle_name'];
    lastName = json['last_name'];
    birthday = json['birthday'];
    timezone = json['timezone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['middle_name'] = this.middleName;
    data['last_name'] = this.lastName;
    data['birthday'] = this.birthday;
    data['timezone'] = this.timezone;
    return data;
  }
}
