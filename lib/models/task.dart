class Task {
  String? name;
  String? description;
  String? status;
  String? datetimeStart;
  String? datetimeEnd;
  String? id;
  String? userId;
  String? workAreaId;
  String? ownerId;
  List? workingTools;
  String? createdAt;
  String? updatedAt;

  Task(
      {this.name,
      this.description,
      this.status,
      this.datetimeStart,
      this.datetimeEnd,
      this.id,
      this.userId,
      this.workAreaId,
      this.ownerId,
      this.workingTools,
      this.createdAt,
      this.updatedAt});

  Task.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    description = json['description'];
    status = json['status'];
    datetimeStart = json['datetime_start'];
    datetimeEnd = json['datetime_end'];
    id = json['id'];
    userId = json['user_id'];
    workAreaId = json['work_area_id'];
    ownerId = json['owner_id'];
    workingTools = json['working_tools'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['description'] = this.description;
    data['status'] = this.status;
    data['datetime_start'] = this.datetimeStart;
    data['datetime_end'] = this.datetimeEnd;
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['work_area_id'] = this.workAreaId;
    data['owner_id'] = this.ownerId;
    data['working_tools'] = this.workingTools;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
