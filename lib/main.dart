import 'package:agrocode_hack_mobile/index.dart';
import 'package:agrocode_hack_mobile/modules/auth/cubit/cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() async {
  await Storage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthCubit(
              AuthApi(DioClient(onRefreshFailure: () {})),
              UserApi(DioClient(onRefreshFailure: () {}))),
        ),
        /*  BlocProvider(
          create: (context) => SubjectBloc(),
        ), */
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme:
              ColorScheme.fromSeed(seedColor: ThemeColor.mainMaterialColor),
          useMaterial3: true,
        ),
        initialRoute: Initial.routeName,
        onGenerateRoute: RouterCfg.generateRoute,
        home: const Initial(),
      ),
    );
  }
}
/* 
class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: InfoBox(
                    title: 'sjkndc',
                    description: 'isnvs',
                  ),
                ),
              ],
            ),
            SizedBox(height: 15.0),
            OrderTile(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          openBS(
            context: context,
            heightBS: MediaQuery.of(context).size.height * 0.7,
            width: double.infinity,
            child: const Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: InfoBox(
                        title: 'Операция',
                        description: 'Обработка почвы',
                      ),
                    ),
                    SizedBox(width: 8.0),
                    Expanded(
                      child: InfoBox(
                        title: 'Поле',
                        description: 'А-121-АБ',
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
 */