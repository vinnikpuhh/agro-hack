import 'package:agrocode_hack_mobile/index.dart';

class WorkAreaApi extends ApiBase {
  final DioClient _client;
  WorkAreaApi(this._client);

  Future<Result<WorkArea>> createWorkArea(
    String name,
    String description,
    String latitude,
    String longitude,
    String area_size,
    String units,
  ) async {
    return run(() async {
      final res = await _client.post('/work-area/create/', data: {
        "name": name,
        "description": description,
        "latitude": latitude,
        "longitude": longitude,
        "area_size": area_size,
        "units": units,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          WorkArea.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> workAreaUpdate(
    String id,
    String name,
    String description,
    String latitude,
    String longitude,
    String area_size,
    String units,
  ) async {
    return run(() async {
      final res = await _client.put('/work-area/update/$id', data: {
        "name": name,
        "description": description,
        "latitude": latitude,
        "longitude": longitude,
        "area_size": area_size,
        "units": units,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> workAreaDelete(
    String id,
  ) async {
    return run(() async {
      final res = await _client.delete('/work-area/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<WorkArea>> getWorkArea(
    String id,
  ) async {
    return run(() async {
      final res = await _client.get('/work-area/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          WorkArea.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<List<WorkArea>>> getWorkAreaList() async {
    return run(() async {
      final res = await _client.get('/work-area/list/');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success((res.data['work_areas'] as List)
            .map((i) => WorkArea.fromJson(i))
            .toList());
      }
      throw Exception('Ошибка');
    });
  }
}
