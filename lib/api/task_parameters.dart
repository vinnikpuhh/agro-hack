import 'package:agrocode_hack_mobile/index.dart';

class TaskParametersApi extends ApiBase {
  final DioClient _client;
  TaskParametersApi(this._client);

  Future<Result<TaskParameters>> createTaskParameters(
    String name,
    String value,
    String units,
  ) async {
    return run(() async {
      final res = await _client.post('/task-parameters/create/', data: {
        "name": name,
        "value": value,
        "units": units,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          TaskParameters.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> updateTaskParameters(
    String id,
    String name,
    String value,
    String units,
  ) async {
    return run(() async {
      final res = await _client.put('/task-parameters/update/$id', data: {
        "name": name,
        "value": value,
        "units": units,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> deleteTaskParameters(
    String id,
  ) async {
    return run(() async {
      final res = await _client.delete('/task-parameters/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<TaskParameters>> getTaskParameters(
    String id,
  ) async {
    return run(() async {
      final res = await _client.get('/task-parameters/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          TaskParameters.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<List<TaskParameters>>> getTaskParametersList() async {
    return run(() async {
      final res = await _client.get('/task-parameters/list/');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success((res.data['task_parameters'] as List)
            .map((i) => TaskParameters.fromJson(i))
            .toList());
      }
      throw Exception('Ошибка');
    });
  }
}
