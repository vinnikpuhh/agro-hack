import 'package:agrocode_hack_mobile/index.dart';

class UserApi extends ApiBase {
  final DioClient _client;
  UserApi(this._client);

  Future<Result<User>> me() async {
    return run(() async {
      final res = await _client.get('/me');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          User.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<User>> userCreate(
    String email,
    String username,
    String phone_number,
    String password,
    String role,
    bool is_active,
    String first_name,
    String middle_name,
    String last_name,
    DateTime birthday,
    String timezone,
  ) async {
    return run(() async {
      final res = await _client.post('/user/create', data: {
        {
          "email": email,
          "username": username,
          "phone_number": phone_number,
          "password": password,
          "role": role,
          "is_active": is_active,
          "profile": {
            "first_name": first_name,
            "middle_name": middle_name,
            "last_name": last_name,
            "birthday": birthday,
            "timezone": timezone,
          }
        }
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          User.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> userUpdate(
    String id,
    String email,
    String username,
    String phone_number,
    String password,
    String role,
    bool is_active,
    String first_name,
    String middle_name,
    String last_name,
    DateTime birthday,
    String timezone,
  ) async {
    return run(() async {
      final res = await _client.put(
        '/user/update/$id',
        data: {
          "email": email,
          "username": username,
          "phone_number": phone_number,
          "password": password,
          "role": role,
          "is_active": is_active,
          "profile": {
            "first_name": first_name,
            "middle_name": middle_name,
            "last_name": last_name,
            "birthday": birthday,
            "timezone": timezone,
          }
        },
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> userDelete(String id) async {
    return run(() async {
      final res = await _client.delete(
        '/user/delete/$id',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<User>> userGet(String id) async {
    return run(() async {
      final res = await _client.get(
        '/user/$id',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          User.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<List<User>>> userList(String id) async {
    return run(() async {
      final res = await _client.get(
        '/user/list/',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
            (res.data as List).map((i) => User.fromJson(i)).toList());
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> addTaskForUser(String id, String taskid) async {
    return run(() async {
      final res = await _client.post(
        '/user/$id/add-task/$taskid/',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> removeTaskForUser(String id, String taskid) async {
    return run(() async {
      final res = await _client.post(
        '/user/$id/add-task/$taskid/',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<List<Task>>> getTasksUser(String id) async {
    return run(() async {
      final res = await _client.get(
        '/user/$id/get-tasks/',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
            (res.data['tasks'] as List).map((i) => Task.fromJson(i)).toList());
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<List<WorkingTools>>> getWorkingTools() async {
    return run(() async {
      final res = await _client.get(
        '/user/get-working-tools/',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success((res.data['working_tools'] as List)
            .map((i) => WorkingTools.fromJson(i))
            .toList());
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> addWorkingToolsForUser(
      String id, String workingToolsid) async {
    return run(() async {
      final res = await _client.post(
        '/user/$id/add-working-tools/$workingToolsid',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> removeWorkingToolsForUser(
      String id, String workingToolsid) async {
    return run(() async {
      final res = await _client.post(
        '/user/$id/remove-working-tools/$workingToolsid',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<Task>> getClosestTask() async {
    return run(() async {
      final res = await _client.get(
        '/user/get-closest-task/',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          Task.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }
}
