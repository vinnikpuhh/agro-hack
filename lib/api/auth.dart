import 'dart:convert';

import 'package:agrocode_hack_mobile/index.dart';

class AuthApi extends ApiBase {
  final DioClient _client;
  AuthApi(this._client);

  Future<Result<TokenResponse>> auth(
    String login,
    String password,
  ) async {
    return run(() async {
      final res = await _client.post('/login', queryParameters: {
        /*       'grant_type': '',
        'scope': '',
        'client_id': '',
        'client_secret': '', */
        'username': login,
        'password': password,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          TokenResponse.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }
}
