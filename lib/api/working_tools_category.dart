import 'package:agrocode_hack_mobile/index.dart';

class WorkingToolsCategoryApi extends ApiBase {
  final DioClient _client;
  WorkingToolsCategoryApi(this._client);

  Future<Result<WorkingToolsCategory>> createWorkingToolsCategory(
    String name,
  ) async {
    return run(() async {
      final res = await _client.post('/working-tools-category/create/', data: {
        "name": name,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          WorkingToolsCategory.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> updateWorkingToolsCategory(
    String id,
    String name,
  ) async {
    return run(() async {
      final res =
          await _client.put('/working-tools-category/update/$id', data: {
        "name": name,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> deleteWorkingToolsCategory(
    String id,
  ) async {
    return run(() async {
      final res = await _client.delete('/working-tools-category/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<WorkingToolsCategory>> getWorkingToolsCategory(
    String id,
  ) async {
    return run(() async {
      final res = await _client.get('/working-tools-category/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          WorkingToolsCategory.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<List<WorkingToolsCategory>>>
      getWorkingToolsCategoryList() async {
    return run(() async {
      final res = await _client.get('/working-tools-category/list/');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success((res.data['working_tools_categories'] as List)
            .map((i) => WorkingToolsCategory.fromJson(i))
            .toList());
      }
      throw Exception('Ошибка');
    });
  }
}
