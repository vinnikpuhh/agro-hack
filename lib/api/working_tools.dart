import 'package:agrocode_hack_mobile/index.dart';

class WorkingToolsApi extends ApiBase {
  final DioClient _client;
  WorkingToolsApi(this._client);

  Future<Result<WorkingTools>> createWorkingTools(
    String name,
    String user_id,
    String category_id,
  ) async {
    return run(() async {
      final res = await _client.post('/working-tools/create/',
          data: {"name": name, "user_id": user_id, "category_id": category_id});
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          WorkingTools.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> workingToolsUpdate(
    String id,
    String name,
    String description,
    String latitude,
    String longitude,
    String area_size,
    String units,
  ) async {
    return run(() async {
      final res = await _client.put('/working-tools/update/$id', data: {
        "name": name,
        "description": description,
        "latitude": latitude,
        "longitude": longitude,
        "area_size": area_size,
        "units": units,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> workingToolsaDelete(
    String id,
  ) async {
    return run(() async {
      final res = await _client.delete('/working-tools/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<WorkingTools>> getWorkingTools(
    String id,
  ) async {
    return run(() async {
      final res = await _client.get('/working-tools/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          WorkingTools.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<List<WorkingTools>>> getWorkingToolsList() async {
    return run(() async {
      final res = await _client.get('/working-tools/list/');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success((res.data['working_tools'] as List)
            .map((i) => WorkingTools.fromJson(i))
            .toList());
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> setCategoryForWorkingTool(String id, String categoryId) async {
    return run(() async {
      final res =
          await _client.get('/working-tools/$id/set-category/$categoryId');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success((res.data['working_tools'] as List)
            .map((i) => WorkingTools.fromJson(i))
            .toList());
      }
      throw Exception('Ошибка');
    });
  }
}
