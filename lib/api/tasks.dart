import 'package:agrocode_hack_mobile/index.dart';

class TasksApi extends ApiBase {
  final DioClient _client;
  TasksApi(this._client);

  Future<Result<Task>> createTask(
    String name,
    String description,
    String status,
    DateTime datetime_start,
    DateTime datetime_end,
  ) async {
    return run(() async {
      final res = await _client.post('/task/create/', data: {
        "name": name,
        "description": description,
        "status": status,
        "datetime_start": datetime_start,
        "datetime_end": datetime_end,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          Task.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> updateTask(
    String id,
    String name,
    String description,
    String status,
    DateTime datetime_start,
    DateTime datetime_end,
  ) async {
    return run(() async {
      final res = await _client.put('/task/update/$id', data: {
        "name": name,
        "description": description,
        "status": status,
        "datetime_start": datetime_start,
        "datetime_end": datetime_end,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          res.data,
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> daleteTask(
    String id,
  ) async {
    return run(() async {
      final res = await _client.delete(
        '/task/$id',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          res.data,
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<Task>> getTask(
    String id,
  ) async {
    return run(() async {
      final res = await _client.get(
        '/task/$id',
      );
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          Task.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<List<Task>>> getTaskList() async {
    return run(() async {
      final res = await _client.get('/task/list/');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
            (res.data['tasks'] as List).map((i) => Task.fromJson(i)).toList());
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> addWorkingToolForTask(
    String id,
    String workingToolId,
  ) async {
    return run(() async {
      final res =
          await _client.post('/task/$id/add-working-tools/$workingToolId');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> removeWorkingToolFromTask(
    String id,
    String workingToolId,
  ) async {
    return run(() async {
      final res =
          await _client.post('/task/$id/remove-working-tools/$workingToolId');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> addWorkAreaForTask(
    String id,
    String workAreaId,
  ) async {
    return run(() async {
      final res = await _client.post('/task/$id/add-work-area/$workAreaId');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> removeWorkAreaFromTask(
    String id,
    String workAreaId,
  ) async {
    return run(() async {
      final res = await _client.post('/task/$id/remove-work-area/$workAreaId');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> addTaskParameter(
    String id,
    String taskParametrId,
  ) async {
    return run(() async {
      final res =
          await _client.post('/task/$id/add-task-parameter/$taskParametrId');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> removeTaskParameter(
    String id,
    String taskParametrId,
  ) async {
    return run(() async {
      final res =
          await _client.post('/task/$id/remove-task-parameter/$taskParametrId');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }
}
