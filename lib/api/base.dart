import 'package:agrocode_hack_mobile/index.dart';
import 'package:dio/dio.dart';

abstract class ApiBase {
  Future<Result<T>> run<T>(Future<Result<T>> Function() fn) async {
    try {
      return await fn();
    } on DioException catch (e) {
      if (e.response?.data?['messages'] != null) {
        return Result.failure(e.response!.data['messages'][0]);
      } else if (e.response?.data?['message'] != null) {
        return Result.failure(e.response!.data['message']);
      } else {
        return Result.failure(ErrorModel(
            errorMessage: e.response?.data['error'] ?? '', errorId: -1, id: 0));
      }
    } catch (e) {
      return Result.failure(
          ErrorModel(errorId: -1, id: -1, errorMessage: e.toString()));
    }
  }
}

class Result<T> {
  final bool success;
  final ErrorModel? error;
  final T? data;

  Result._({
    required this.success,
    this.error,
    this.data,
  });

  factory Result.success(T data) {
    return Result._(success: true, data: data);
  }

  factory Result.failure(ErrorModel model) {
    return Result._(success: false, error: model);
  }
}
