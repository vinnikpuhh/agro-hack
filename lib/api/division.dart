import 'package:agrocode_hack_mobile/index.dart';

class DivisionApi extends ApiBase {
  final DioClient _client;
  DivisionApi(this._client);

  Future<Result<Division>> createDivision(
    String name,
  ) async {
    return run(() async {
      final res = await _client.post('/division/create/', data: {
        'name': name,
      });
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          Division.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> divisionUpdate(
    String id,
  ) async {
    return run(() async {
      final res = await _client.put('/division/update/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> divisionDelete(
    String id,
  ) async {
    return run(() async {
      final res = await _client.delete('/division/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<Division>> getDivision(
    String id,
  ) async {
    return run(() async {
      final res = await _client.get('/division/$id');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(
          Division.fromJson(res.data),
        );
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result<List<Division>>> getDivisionList() async {
    return run(() async {
      final res = await _client.get('/division/list/');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success((res.data['divisions'] as List)
            .map((i) => Division.fromJson(i))
            .toList());
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> addUserInDivision(
    String idDivision,
    String idUser,
  ) async {
    return run(() async {
      final res = await _client.post('/division/$idDivision/add-user/$idUser');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> removeUserFromDivision(
    String idDivision,
    String idUser,
  ) async {
    return run(() async {
      final res =
          await _client.post('/division/$idDivision/remove-user/$idUser');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> addWorkAreaInDivision(
    String idDivision,
    String idWorkArea,
  ) async {
    return run(() async {
      final res =
          await _client.post('/division/$idDivision/add-work-area/$idWorkArea');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> removeWorkAreaFromDivision(
    String idDivision,
    String idWorkArea,
  ) async {
    return run(() async {
      final res = await _client
          .post('/division/$idDivision/remove-work-area/$idWorkArea');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> addWorkToolsInDivision(
    String idDivision,
    String idWorkingTools,
  ) async {
    return run(() async {
      final res = await _client
          .post('/division/$idDivision/add-working-tools/$idWorkingTools');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }

  Future<Result> removeWorkToolsFromDivision(
    String idDivision,
    String idWorkingTools,
  ) async {
    return run(() async {
      final res = await _client
          .post('/division/$idDivision/remove-working-tools/$idWorkingTools');
      final statusCode = res.statusCode ?? -1;
      if (statusCode > 199 && statusCode < 300) {
        return Result.success(res.data);
      }
      throw Exception('Ошибка');
    });
  }
}
