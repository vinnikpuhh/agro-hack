import 'package:agrocode_hack_mobile/index.dart';
import 'package:agrocode_hack_mobile/utils/storage.dart';
import 'package:bloc/bloc.dart';

part 'state.dart';

class AuthCubit extends Cubit<AuthentficationState> {
  final UserApi userApi;
  final AuthApi authApi;
  AuthCubit(this.authApi, this.userApi)
      : super(AuthentficationState(
          login: '',
          status: Authentfication.init,
          password: '',
        ));

  Future<void> sendRequest(String login, String password) async {
    emit(state.settings(status: Authentfication.loading));

    final result = await authApi.auth(login, password);

    if (!result.success || result.data == null) {
      emit(
        state.settings(
          status: Authentfication.failure,
        ),
      );
      return;
    }
    /*   final res = await userApi.me();
    if (!res.success || res.data == null) {
      emit(
        state.settings(
          status: Authentfication.failure,
        ),
      );
      return;
    } */
    Storage.shared.setRefreshToken(result.data!.refreshToken!);
    Storage.shared.setToken(result.data!.token!);
    emit(
      state.settings(
        status: Authentfication.success,
      ),
    );
  }
}
