part of 'cubit.dart';

enum Authentfication {
  init,
  loading,
  success,
  failure,
}

extension AuthentficationX on Authentfication {
  bool get init => this == Authentfication.init;
  bool get loading => this == Authentfication.loading;
  bool get success => this == Authentfication.success;
  bool get failure => this == Authentfication.failure;
}

class AuthentficationState {
  String login;
  final Authentfication status;
  String password;
  AuthentficationState({
    required this.login,
    required this.status,
    required this.password,
  });

  AuthentficationState settings({
    String? login,
    Authentfication? status,
    String? password,
  }) {
    return AuthentficationState(
      login: login ?? this.login,
      status: status ?? this.status,
      password: password ?? this.password,
    );
  }
}
