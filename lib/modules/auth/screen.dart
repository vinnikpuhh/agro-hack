import 'package:agrocode_hack_mobile/index.dart';
import 'package:agrocode_hack_mobile/modules/auth/cubit/cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class AuthView extends StatefulWidget {
  const AuthView({super.key});

  static const String routeName = '/auth';

  @override
  State<AuthView> createState() => _AuthViewState();
}

class _AuthViewState extends State<AuthView> {
  TextEditingController loginCtr = TextEditingController();
  TextEditingController passCtr = TextEditingController();

  FocusNode loginFocus = FocusNode();
  FocusNode passFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthCubit, AuthentficationState>(
      listener: (context, state) {
        if (state.status.success) {
          Navigator.pushNamed(context, ActualTaskView.routeName);
        }
        if (state.status.failure) {
          showTopSnackBar(
            Overlay.of(context),
            CustomSnackBar.error(
              message: "Что-то пошло не так",
            ),
          );
        }
      },
      builder: (context, state) {
        return CustomScaffold(
          currentRoute: AuthView.routeName,
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  'Добро пожаловать',
                  style: StyleText.h1Bold,
                ),
                SizedBox(
                  height: 24.0,
                ),
                Text(
                  'Логин',
                  style: StyleText.normalSmall
                      .copyWith(color: ThemeColor.darkText),
                ),
                SizedBox(
                  height: 5.0,
                ),
                TextField(
                  autofocus: false,
                  controller: loginCtr,
                  focusNode: loginFocus,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text(
                  'Логин',
                  style: StyleText.normalSmall
                      .copyWith(color: ThemeColor.darkText),
                ),
                SizedBox(
                  height: 5.0,
                ),
                TextField(
                  autofocus: false,
                  controller: passCtr,
                  focusNode: passFocus,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 24.0,
                ),
                BlocBuilder<AuthCubit, AuthentficationState>(
                  builder: (context, st) {
                    return ActionButton(
                        color: ThemeColor.action,
                        title: 'Войти',
                        loading: st.status == Authentfication.loading,
                        onTap: () async {
                          BlocProvider.of<AuthCubit>(context)
                              .sendRequest(loginCtr.text, passCtr.text);
                          setState(() {});
                        });
                  },
                ),
                SizedBox(
                  height: 12.0,
                ),
                ActionButton(
                    color: ThemeColor.mainMaterialColor,
                    title: 'Войти по NFC',
                    onTap: () {}),
              ],
            ),
          ),
        );
      },
    );
  }
}
