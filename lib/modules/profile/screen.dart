import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({super.key});

  static const String routeName = '/profile';
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      currentRoute: ProfileView.routeName,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 24.0,
            left: 24.0,
            right: 24.0,
          ),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    height: 60.0,
                    width: 60.0,
                    decoration: BoxDecoration(
                      color: Colors.amber,
                      borderRadius: BorderRadius.circular(60.0),
                    ),
                  ),
                  const Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10.0,
                      ),
                      child: Text(
                        'Морева\nСветлана',
                        style: StyleText.h1Bold,
                      ),
                    ),
                  ),
                  SvgPicture.asset(
                    SvgIcon.notify,
                  ),
                ],
              ),
              const SizedBox(height: 24.0),
              const Row(
                children: [
                  Expanded(
                      child: InfoBox(
                          title: 'За месяц обработано Га', description: '74')),
                ],
              ),
              const SizedBox(height: 8.0),
              const Row(
                children: [
                  Expanded(
                      child: InfoBox(title: 'Оплата', description: '180 000р')),
                ],
              ),
              const SizedBox(height: 8.0),
              const Row(
                children: [
                  Expanded(
                      child:
                          InfoBox(title: 'Категория прав', description: 'С')),
                ],
              ),
              const SizedBox(height: 8.0),
              Container(
                padding: const EdgeInsets.all(16.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16.0),
                  color: ThemeColor.backgroundTile,
                ),
                child: const Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Последняя задача',
                            style: StyleText.normalMedium,
                          ),
                          Text(
                            'RSM 3575',
                            style: StyleText.h1Bold,
                          ),
                          Text(
                            'Quivogn SSD 4',
                            style: StyleText.normalSmall,
                          ),
                          Text(
                            '32 000p',
                            style: StyleText.normalMedium,
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: InfoBox(
                                  title: 'title',
                                  description: 'description',
                                  customColor: ThemeColor.warning,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8.0),
                          Row(
                            children: [
                              Expanded(
                                child: InfoBox(
                                  title: 'title',
                                  description: 'description',
                                  customColor: ThemeColor.warning,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 8.0),
                          Row(
                            children: [
                              Expanded(
                                child: InfoBox(
                                  title: 'title',
                                  description: 'description',
                                  customColor: ThemeColor.warning,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 24.0),
              Row(
                children: [
                  SvgPicture.asset(SvgIcon.exit),
                  const SizedBox(width: 8.0),
                  const Text(
                    'Выйти',
                    style: StyleText.normalLarge,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
