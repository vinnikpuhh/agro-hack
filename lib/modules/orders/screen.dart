import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OrdersView extends StatelessWidget {
  const OrdersView({super.key});

  static const String routeName = '/orders';
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      currentRoute: OrdersView.routeName,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 24.0,
            left: 24.0,
            right: 24.0,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Задачи на сегодня',
                    style: StyleText.h1Bold,
                  ),
                  SvgPicture.asset(SvgIcon.calendar),
                ],
              ),
              /*  const Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Row(
                  children: [
                    CustomChip(
                      title: 'Сегодня',
                    ),
                    SizedBox(width: 4.0),
                    CustomChip(
                      title: 'Завтра',
                    ),
                    SizedBox(width: 4.0),
                    CustomChip(
                      title: 'Послезавтра',
                    ),
                  ],
                ),
              ), */
              const Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: 8.0),
                      OrderTile(),
                      SizedBox(height: 8.0),
                      OrderTile(),
                      SizedBox(height: 8.0),
                      OrderTile(),
                      SizedBox(height: 8.0),
                      OrderTile(),
                      SizedBox(height: 8.0),
                      OrderTile(),
                      SizedBox(height: 8.0),
                      OrderTile(),
                      SizedBox(height: 8.0),
                      OrderTile(),
                      SizedBox(height: 8.0),
                      OrderTile(),
                      SizedBox(height: 8.0),
                      OrderTile(),
                      SizedBox(height: 8.0),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
