import 'package:agrocode_hack_mobile/api/task_parameters.dart';
import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'index.dart';

class ActualTaskCubit extends Cubit<ActualTaskState> {
  final UserApi userApi;
  final WorkAreaApi workArea;

  ActualTaskCubit(
    this.userApi,
    this.workArea,
  ) : super(ActualTaskState(
          task: null,
          status: ActualTaskStatus.init,
          workArea: null,
          parameters: null,
        ));

  Future<void> getLastTask() async {
    emit(state.settings(status: ActualTaskStatus.loading));

    final result = await userApi.getClosestTask();

    if (!result.success || result.data == null) {
      emit(state.settings(status: ActualTaskStatus.failure));
    }
    final res = await workArea.getWorkArea(result.data?.workAreaId ?? '');
    if (!res.success || res.data == null) {
      emit(state.settings(status: ActualTaskStatus.failure));
    }

    final r = await workArea.getWorkArea(result.data?.workAreaId ?? '');
    if (!res.success || res.data == null) {
      emit(state.settings(status: ActualTaskStatus.failure));
    }
    emit(
      state.settings(
        status: ActualTaskStatus.success,
        task: result.data,
        workArea: res.data,
      ),
    );
  }
}
