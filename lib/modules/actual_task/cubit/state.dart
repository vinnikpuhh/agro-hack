import 'package:agrocode_hack_mobile/index.dart';

enum ActualTaskStatus {
  init,
  loading,
  success,
  failure,
}

extension ActualTaskStatusX on ActualTaskStatus {
  bool get init => this == ActualTaskStatus.init;
  bool get loading => this == ActualTaskStatus.loading;
  bool get success => this == ActualTaskStatus.success;
  bool get failure => this == ActualTaskStatus.failure;
}

class ActualTaskState {
  Task? task;
  WorkArea? workArea;
  TaskParameters? parameters;
  final ActualTaskStatus status;
  ActualTaskState({
    required this.task,
    required this.workArea,
    required this.status,
    required this.parameters,
  });

  ActualTaskState settings({
    Task? task,
    WorkArea? workArea,
    ActualTaskStatus? status,
    TaskParameters? parameters,
  }) {
    return ActualTaskState(
      parameters: parameters ?? this.parameters,
      task: task ?? this.task,
      status: status ?? this.status,
      workArea: workArea ?? this.workArea,
    );
  }
}
