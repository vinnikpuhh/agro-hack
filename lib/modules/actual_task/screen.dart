import 'package:agrocode_hack_mobile/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cubit/index.dart';

class ActualTaskView extends StatelessWidget {
  const ActualTaskView({super.key});

  static const String routeName = '/actualTaskView';
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ActualTaskCubit(
          UserApi(
            DioClient(
              onRefreshFailure: () {},
            ),
          ),
          WorkAreaApi(DioClient(onRefreshFailure: () {})))
        ..getLastTask(),
      child: BlocBuilder<ActualTaskCubit, ActualTaskState>(
        builder: (context, state) {
          if (state.status.loading) {
            return Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ));
          }
          if (state.status.failure) {
            return Container(
                color: Colors.white,
                child: Center(
                  child: ActionButton(
                    color: ThemeColor.action,
                    title: '',
                    onTap: () {},
                  ),
                ));
          }
          return CustomScaffold(
            currentRoute: ActualTaskView.routeName,
            child: Stack(
              children: [
                MapGoogle(workArea: state.workArea),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    onTap: () => openBS(
                      context: context,
                      heightBS: MediaQuery.of(context).size.height * 0.65,
                      width: double.infinity,
                      child: bs(context),
                    ),
                    child: Container(
                      margin: const EdgeInsets.all(21.0),
                      padding: const EdgeInsets.symmetric(
                          vertical: 16.0, horizontal: 25.0),
                      height: 140.0,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                          32.0,
                        ),
                        color: Colors.white,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            state.task?.name ?? '',
                            style: StyleText.boldLarge,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Посев',
                                style: StyleText.normalLarge,
                              ),
                              Text(
                                '13:00',
                                style: StyleText.normalLarge,
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Text(
                                    'Операция',
                                    style: StyleText.normalSmall,
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Text(
                                    'Обработка почвы',
                                    style: StyleText.normalMedium,
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Text(
                                    'Поле',
                                    style: StyleText.normalSmall,
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Text(
                                    'А-121-АБ',
                                    style: StyleText.normalMedium,
                                  ),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  Widget bs(BuildContext context) {
    return Column(
      children: [
        Text('Задача 3'),
        SizedBox(
          height: 16.0,
        ),
        Row(
          children: [
            Expanded(
              child: InfoBox(title: 'Поле', description: 'А-121-АБ'),
            ),
            SizedBox(width: 8.0),
            Expanded(
                flex: 2, child: InfoBox(title: 'Задание', description: 'Посев'))
          ],
        ),
        SizedBox(
          height: 16.0,
        ),
        Row(
          children: [
            Expanded(
              child: InfoBox(title: 'Техника', description: 'RSM 3575'),
            ),
            SizedBox(width: 8.0),
            Expanded(
                flex: 2,
                child: InfoBox(title: 'Агрегат', description: 'Quivogn SSD 4'))
          ],
        ),
        SizedBox(
          height: 16.0,
        ),
        Row(
          children: [
            Expanded(
              child: InfoBox(title: 'Глубина', description: '8 См'),
            ),
          ],
        ),
        SizedBox(
          height: 16.0,
        ),
        Row(
          children: [
            Expanded(
              child: InfoBox(title: 'Скорость', description: '12 Км/Ч'),
            ),
          ],
        ),
        SizedBox(
          height: 16.0,
        ),
        Row(
          children: [
            Expanded(
              child: InfoBox(
                  title: 'Комментарий',
                  description: 'Участок 12, рядом с воротами'),
            ),
          ],
        ),
        SizedBox(
          height: 16.0,
        ),
        ActionButton(
            color: ThemeColor.action,
            title: 'Закрыть',
            onTap: () {
              Navigator.pop(context);
            })
      ],
    );
  }
}
